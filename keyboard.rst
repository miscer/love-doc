Keyboard
========

Example::
  
  love.keyboard.on('pressed', function(key) {
    switch (key) {
      case 'left':
      case 'right':
        go(key);
        break;
      case 'up':
        jump();
        break;
    }
  });

``love.keyboard``
-----------------

.. js:function:: love.keyboard.isDown(key)
   
   :returns: bool

Events
------

.. js:function:: pressed(key, code)

   Triggered when key is pressed.

.. js:function:: released(key, code)
   
   Triggered when key is released.

Key names
---------

* **arrows**: up, down, left, right
* **spacebar**: [actual space character]
* **letters**: a - z
* **numbers**: 0 - 9
* **keypad numbers**: kp0 - kp9
* **keypad keys**: kp/, kp*, kp-, kp+, kp.
* **characters**: ; = , - . / ` [ \ ] '
* **modifier keys**: shift, ctrl, alt, capslock
* **editing keys**: backspace, tab, return, insert, delete
* **navigation keys**: pageup, pagedown, end, home
* menu, esc
