Helpers
=======

Detecting types
---------------

.. js:function:: love.helpers.is.number(obj)
   
   Checks whether the object is a number, but not NaN or Infinity.

.. js:function:: love.helpers.is.string(obj)

   Checks whether the object is a string.

.. js:function:: love.helpers.is.canvas(obj)
   
   Checks whether the object is canvas element.
