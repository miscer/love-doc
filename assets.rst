Loading assets
==============

``love.assets``
---------------

.. js:function:: love.assets.get(url)

   :param string url: An URL to the asset
   :returns: Asset

.. js:function:: love.assets.get(base, ext1, ext2, ...)

   :param string base: Name part of URL
   :param string ext1: Extension part of URL for first source
   :param string ext2: Extension part of URL for second source
   :returns: Asset

   This variant of the ``love.assets.get`` function is used when the asset
   has more formats. For example, with audio you can't use just one format,
   because there isn't one that would be supported in every browser.

   In that case, you just specify all the extensions that your asset is
   available in::

       var music = love.assets.get('music/menu', '.mp3', '.ogg');

.. js:function:: love.assets.add

   This function is equal to ``love.assets.get``, except it doesn't return
   the asset. This is useful only when you don't need the asset in code,
   but you need it loaded, for example to use in CSS.

.. js:function:: love.assets.load(callback)
   
   :param callback: Called when all assets are loaded

Asset
-----

Asset object is returned by the ``love.assets.get`` function.

.. js:function:: Asset.prototype.getContent()
   
   :returns: Asset's content, e.g. image or audio

.. js:function:: Asset.prototype.isLoaded()
   
   :returns: Whether the asset is loaded
