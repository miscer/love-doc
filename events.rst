Events
======

``love.events``
---------------

.. js:function:: love.events.extend(object)
   
   Extends ``object`` with event methods. This should be used in constructor,
   for example::

       function Foo() {
         love.events.extend(this);
       }

Eventified object
-----------------

.. js:function:: object.on(event, listener)

   Attaches ``listener`` to an ``event``.

.. js:function:: object.once(event, listener)

   Attaches ``listener`` to ``event`` and removes it after it was called.

.. js:function:: object.trigger(event[, args...])

   Calls all the listeners attached to ``event``, with ``args`` as arguments.
